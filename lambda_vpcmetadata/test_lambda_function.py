# pylint: disable=redefined-outer-name
"""Module for testing lambda-function"""

from unittest.mock import Mock, patch, ANY
import pytest
import lambda_function
from lambda_function import lambda_handler
from lambda_function import store_object_into_s3_sse


@pytest.fixture
def fixture_subnet_metadata():
    """Fixture to setup vpc metadata"""

    subnet_01 = {"sn_id": "subnet-11111", "cidr_clock": "10.192.22.0/24", "az": "eu-west-1a"}
    subnet_02 = {"sn_id": "subnet-22222", "cidr_clock": "10.192.21.0/24", "az": "eu-west-1b"}
    subnet_03 = {"sn_id": "subnet-33333", "cidr_clock": "10.192.20.0/24", "az": "eu-west-1c"}

    subnet_metadata = {"sn_01": subnet_01, "sn_02": subnet_02, "sn_03": subnet_03}

    return subnet_metadata


@pytest.mark.usefixtures('fixture_subnet_metadata')
@patch.object(lambda_function, 'store_object_into_s3_sse', return_value=None)
@patch("lambda_function.boto3", autospec=True, spec_set=True)
def test_lambda_function(stub_boto3, mock_store_object_into_s3_sse, fixture_subnet_metadata):
    """ Test a successful lambda function request.
    """

    stub_sn_01 = Mock()
    stub_sn_01.id = fixture_subnet_metadata.get("sn_01")["sn_id"]
    stub_sn_01.cidr_block = fixture_subnet_metadata.get("sn_01")["cidr_clock"]
    stub_sn_01.availability_zone = fixture_subnet_metadata.get("sn_01")["az"]

    stub_sn_02 = Mock()
    stub_sn_02.id = fixture_subnet_metadata.get("sn_01")["sn_id"]
    stub_sn_02.cidr_block = fixture_subnet_metadata.get("sn_02")["cidr_clock"]
    stub_sn_02.availability_zone = fixture_subnet_metadata.get("sn_02")["az"]

    stub_sn_03 = Mock()
    stub_sn_03.id = fixture_subnet_metadata.get("sn_03")["sn_id"]
    stub_sn_03.cidr_block = fixture_subnet_metadata.get("sn_03")["cidr_clock"]
    stub_sn_03.availability_zone = fixture_subnet_metadata.get("sn_03")["az"]

    stub_vpc_01 = Mock()
    stub_vpc_01.subnets.all.return_value = [stub_sn_01, stub_sn_02]

    stub_vpc_02 = Mock()
    stub_vpc_02.subnets.all.return_value = [stub_sn_03]

    ec2_resources = Mock()
    ec2_resources.vpcs.all.return_value = [stub_vpc_01, stub_vpc_02]

    stub_boto3.resource.return_value = ec2_resources

    # No!!!!
    # stub_boto3.resource.vpcs.all.return_value = [stub_vpc_01, stub_vpc_02]

    expected_subnet_json = [
        dict(subnet_id=stub_sn_01.id, cidr_block=stub_sn_01.cidr_block,
             availability_zone=stub_sn_01.availability_zone),
        dict(subnet_id=stub_sn_02.id, cidr_block=stub_sn_02.cidr_block,
             availability_zone=stub_sn_02.availability_zone),
        dict(subnet_id=stub_sn_03.id, cidr_block=stub_sn_03.cidr_block,
             availability_zone=stub_sn_03.availability_zone)]

    lambda_handler({}, {})

    # assert expected_subnet_json == lambda_result
    stub_boto3.resource.assert_called_once_with("ec2")
    mock_store_object_into_s3_sse.assert_called_once_with(ANY, ANY, expected_subnet_json)


@patch("lambda_function.boto3", autospec=True, spec_set=True)
def test_exception_lambda_handler(stub_boto3):
    """Test a lambda function request exception."""

    stub_boto3.resource.side_effect = Exception

    with pytest.raises(Exception):
        lambda_handler({}, {})

    stub_boto3.resource.assert_called_once_with("ec2")


@patch("lambda_function.boto3", autospec=True, spec_set=True)
def test_store_object_into_s3_sse(stub_boto3):
    """ Test a successful s3 storing."""

    # input
    bucket_name = "s3-bucket-name"
    key = "object-key"
    value = "object-value"

    expected_result = "s3.Object(bucket_name='" + bucket_name + "', key='" + key + "')"

    mock_bucket = Mock()
    mock_bucket.put_object.return_value = expected_result

    mock_s3_resource = Mock()
    mock_s3_resource.Bucket.return_value = mock_bucket

    stub_boto3.resource.return_value = mock_s3_resource

    assert expected_result == store_object_into_s3_sse(bucket_name, key, value)
    stub_boto3.resource.assert_called_once_with("s3")


@patch("lambda_function.boto3", autospec=True, spec_set=True)
def test_exception_store_object_into_s3_sse(stub_boto3):
    """ Test a s3 storing exception."""

    mock_bucket = Mock()
    mock_bucket.put_object.side_effect = Exception

    mock_s3_resource = Mock()
    mock_s3_resource.Bucket.return_value = mock_bucket

    stub_boto3.resource.return_value = mock_s3_resource

    with pytest.raises(Exception):
        store_object_into_s3_sse({}, {}, {})

    stub_boto3.resource.assert_called_once_with("s3")
