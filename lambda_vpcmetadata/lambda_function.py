"""Lambda function to retrieve the VPC subnets-metadata and store them in S3 with SSE
The function retrieves all the VPCs and for each of them collects all the subnets metadata:
subnet-id, cidr and availability-zone
The metadata are store in JSON format in S3 and encrypted with SSE
"""

import json
import datetime
import logging
import os
import boto3

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):
    """
    The function retrieve the vpcs-subnet metadata and save them in a S3 bucket with SSE.

    :param event: unused
    :param context: unused
    :except
        in case of exception, a proper message is logged
    """

    try:
        # The array containing the vpc-subnet metadata: subnet_id, cidr_block and cidr_block
        subnet_json = []

        logging.info("Retrieving the vpc-metadata...")

        ec2 = boto3.resource("ec2")
        for vpc in ec2.vpcs.all():
            for subnet in vpc.subnets.all():
                subnet_json.append(dict(
                    subnet_id=subnet.id,
                    cidr_block=subnet.cidr_block,
                    availability_zone=subnet.availability_zone
                ))


        bucket_name = os.environ.get("SubnetMetadataBucket")

        logging.info("bucket name: %s", bucket_name)

        now = datetime.datetime.now()
        key = (now.strftime("%Y-%m-%d_%H:%M:%S") + '_subnet.json')

        logging.info("Calling function to save data in s3... %s", key)

        store_object_into_s3_sse(bucket_name, key, subnet_json)

    except Exception as exception:
        logging.error("Exception running the function %s", exception.args[0])
    else:
        logging.info("End function.")


def store_object_into_s3_sse(bucket_name, key, value):
    # """
    # The function retrieve the vpcs-subnet metadata and save them in a S3 bucket with SSE
    # :param event: unused
    # :param context: unused
    # :except
    #     in case of exception, a proper message is logged
    # """

    """
    The function save in a bucket_name a key-value pair using SSE

    :param bucket_name: str
        S3 bucket where save the key-value object
    :param key: str
        the key object
    :param value: str
        the value object
    :except
        in case of exception, a proper message is logged and the exceptio is raised to the caller
    """

    s3_resource = boto3.resource('s3')

    try:
        logging.info("Saving data in S3...")

        store_result = s3_resource.Bucket(bucket_name).put_object(
            Key=key,
            Body=json.dumps(value),
            ServerSideEncryption='AES256')

        logging.info("Data successfully saved in S3")
    except Exception as exception:
        logging.error("Exception %s", exception)
        raise exception
    else:
        return str(store_result)
