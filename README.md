## 2λ test

The repository contains 2 directories "lambda_resize_img" and "lambda_vpcmetadata"

*"lambda_resize_img"* is related to a λ function for resizing images and then upload them to a S3 bucket.

The directory contains 2 files 

* *lambda_function_tn.py*: the lambda code
* *test_lambda_function.py*: the test code



*"lambda_vpcmetadata"* is related to a λ function for collecting the VPCs subnets-metadata (sn-id, cidr and az) and to store the result in S3 with SSE  

The directory contains 2 files

* *lambda_function.py*: the lambda code
* *test_lambda_function.py*: the test code


The tests have been implemented using Mock framework, pytest and monkey-patching.


In each directory in *"conv_html"*, there is the result of the following command

`pytest --cov-report html:cov_html --cov-branch --cov=<LAMBDA-FUNCTION_without.py> .`

In that directory you can open the file "index.html" in a browser and check the test-coverage result


