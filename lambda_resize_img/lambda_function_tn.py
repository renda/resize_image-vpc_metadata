"""
Lambda function to create a thumbnail
The function is triggered by a s3 upload event, then the function processes the event and it creates
the thumbnail for each images uploaded.
The thumbnails are uploaded in DEST_BUCKET
"""

import os
import logging
import tempfile
import boto3
from PIL import Image

# s3 = boto3.client('s3')
# DEST_BUCKET = os.environ['DEST_BUCKET']
# SIZE = 128, 128

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):
    """
    The lambda function is triggered by a S3 upload event.
    When an image is uploaded the lambda create a thumbnail and save it a a proper dest_bucket

    :param event: dict
        the event that trigger the lambda, it contains the uplopaded file details
    :param context: unused
    :return: thumbnail_created: array
        an array of created thumbnails
    :except
        in case of exception, a proper message is logged
    """

    thumbnail_created = []

    try:
        s3_client = boto3.client('s3')
        dest_bucket = os.environ['DEST_BUCKET']
        size = 128, 128

        for record in event['Records']:
            source_bucket = record['s3']['bucket']['name']
            key = record['s3']['object']['key']
            thumb = 'thumb-' + key
            with tempfile.TemporaryDirectory() as tmpdir:
                download_path = os.path.join(tmpdir, key)
                upload_path = os.path.join(tmpdir, thumb)
                s3_client.download_file(source_bucket, key, download_path)
                generate_thumbnail(download_path, upload_path, size)
                s3_client.upload_file(upload_path, dest_bucket, thumb)

            logging.info('Thumbnail image saved at %s/%s', dest_bucket, thumb)
            thumbnail_created.append(thumb)

    except Exception as exception:
        logging.error("Exception running the function %s)", exception.args[0])
    else:
        logging.info("Thumbnail/s created and stored")
        return thumbnail_created


def generate_thumbnail(source_path, dest_path, size):
    """
    The function creates an image thumbnail
    :param source_path: str
        image source path
    :param dest_path: str
        image destination path
    :param size: tuple
        thumbnail size
    :except
        in case of exception, it will be raised to the caller
    """
    try:
        logging.info('Generating thumbnail from: %s', source_path)
        image = Image.open(source_path)
        image.thumbnail(size)
        image.save(dest_path)
    except Exception as exception:
        logging.error("Exception %s, source_path: %s", exception, source_path)
        raise exception
