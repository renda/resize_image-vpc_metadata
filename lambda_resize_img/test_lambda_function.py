# pylint: disable=redefined-outer-name
"""Module for testing lambda_function_tn"""

from unittest.mock import Mock, patch, call
import pytest
import lambda_function_tn
from lambda_function_tn import lambda_handler
from lambda_function_tn import generate_thumbnail


@pytest.fixture
def fixture_set_up():
    """Fixture to setup testing data"""

    bucket_name = 'bucket-name'
    obj_key_01 = 'object-key 01'
    obj_key_02 = 'object-key 02'

    event_json = \
        {"Records": [{"s3": {"bucket": {"name": bucket_name}, "object": {"key": obj_key_01}}},
                     {"s3": {"bucket": {"name": bucket_name}, "object": {"key": obj_key_02}}}]}

    setup_data = {"event_json": event_json,
                  'source_path': 'source_path',
                  'dest_path': 'dest_path',
                  'dest_bucket': 'dest-bucket',
                  'thumbnail_prefix': 'thumb-',
                  'thumbnail_size': (128, 128),
                  'temp_dir': ['temp_dir_01', 'temp_dir_02']}

    return setup_data


@pytest.mark.usefixtures('fixture_set_up')
@patch.object(lambda_function_tn, 'generate_thumbnail', return_value=None)
@patch("lambda_function_tn.boto3", autospec=True, spec_set=True)
def test_lambda_handler(stub_boto3, mock_generate_thumbnail, fixture_set_up):
    """ Test a successful lambda function request.
    """

    event = fixture_set_up.get("event_json")
    temp_dir = fixture_set_up.get("temp_dir")
    dest_bucket = fixture_set_up.get("dest_bucket")
    thumbnail_size = fixture_set_up.get("thumbnail_size")
    thumbnail_prefix = fixture_set_up.get("thumbnail_prefix")
    bucket_name = event['Records'][0]['s3']['bucket']['name']

    with patch("lambda_function_tn.tempfile", autospec=True, spec_set=True) as mock_tempfile:
        mock_temporary_dir = Mock()
        mock_temporary_dir.__enter__ = Mock(side_effect=[temp_dir[0], temp_dir[1]])
        mock_temporary_dir.__exit__ = Mock(return_value=None)
        mock_tempfile.TemporaryDirectory.return_value = mock_temporary_dir

        with patch("lambda_function_tn.os", autospec=True, spec_set=True) as mock_os:
            mock_os.path.join.side_effect = [temp_dir[0], temp_dir[0], temp_dir[1], temp_dir[1]]
            mock_os.environ.__getitem__.return_value = dest_bucket

            s3_client = stub_boto3.client.return_value = Mock()

            actual_value = lambda_handler(event, {})

            obj_key_01 = event['Records'][0]['s3']['object']['key']
            obj_key_02 = event['Records'][1]['s3']['object']['key']

            # expected_value is like ["thumb-object-key 01", "thumb-object-key 02"]
            expected_value = [thumbnail_prefix + obj_key_01, thumbnail_prefix + obj_key_02]

            assert expected_value == actual_value

            stub_boto3.client.assert_called_once_with("s3")

            calls_os_path_join = \
                [call(temp_dir[0], obj_key_01), call(temp_dir[0], thumbnail_prefix + obj_key_01),
                 call(temp_dir[1], obj_key_02), call(temp_dir[1], thumbnail_prefix + obj_key_02)]
            mock_os.path.join.assert_has_calls(calls_os_path_join)

            calls_generate_thumbnail = [call(temp_dir[0], temp_dir[0], thumbnail_size),
                                        call(temp_dir[1], temp_dir[1], thumbnail_size)]
            mock_generate_thumbnail.assert_has_calls(calls_generate_thumbnail)

            calls_upload_file = [call(temp_dir[0], dest_bucket, thumbnail_prefix + obj_key_01),
                                 call(temp_dir[1], dest_bucket, thumbnail_prefix + obj_key_02)]
            s3_client.upload_file.assert_has_calls(calls_upload_file)

            calls_download_file = [call(bucket_name, obj_key_01, temp_dir[0]),
                                   call(bucket_name, obj_key_02, temp_dir[1])]
            s3_client.download_file.assert_has_calls(calls_download_file)


@patch.dict('lambda_function_tn.os.environ', {'DEST_BUCKET': 's3_dest_bucket'})
@pytest.mark.usefixtures('fixture_set_up')
@patch("lambda_function_tn.tempfile", autospec=True, spec_set=True)
@patch("lambda_function_tn.boto3", autospec=True, spec_set=True)
def test_exception_lambda_handler(stub_boto3, mock_tempfile, fixture_set_up):
    """ Test lambda function request exception.
    """

    event = fixture_set_up.get("event_json")

    mock_tempfile.TemporaryDirectory.side_effect = Exception

    with pytest.raises(Exception):
        lambda_handler(event, {})

    stub_boto3.client.assert_called_once_with("s3")

@pytest.mark.usefixtures('fixture_set_up')
@patch("lambda_function_tn.Image", autospec=True, spec_set=True)
def test_generate_thumbnail(mock_image, fixture_set_up):
    """ Test generate_thumbnail function request.
    """

    source_path = fixture_set_up.get("source_path")
    dest_path = fixture_set_up.get("dest_path")
    size = fixture_set_up.get("size")

    image = Mock()
    mock_image.open.return_value = image

    generate_thumbnail(source_path, dest_path, size)

    mock_image.open.assert_called_once_with(source_path)
    image.thumbnail.assert_called_once_with(size)
    image.save.assert_called_once_with(dest_path)


@pytest.mark.usefixtures('fixture_set_up')
@patch("lambda_function_tn.Image", autospec=True, spec_set=True)
def test_exception_generate_thumbnail(mock_image, fixture_set_up):
    """ Test generate_thumbnail function request exception.
    """

    source_path = fixture_set_up.get("source_path")

    mock_image.open.side_effect = Exception

    with pytest.raises(Exception):
        generate_thumbnail(source_path, {}, {})

    mock_image.open.assert_called_once_with(source_path)
